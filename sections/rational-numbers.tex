% Copyright 2016 Liber Mathematicae, licensed under GNU FDL v1.3
% main author: 
%   Markus Pflaum 
%
\section{Rational numbers}
\para 
Even though the integers form an abelian group with respect to addition, multiplicative 
inverses of integers $n \ne 1,-1$ do not exist in $\Z$. The argument is as follows. 
First observe that $0$ is not multiplicatively invertible in $\Z$ and even not in 
any extension ring $R$ of $\Z$ because if it were with inverse $m$,  
then $1 = 0 \cdot m = 0$ and 
$r = 1 \cdot r = 0 \cdot r = 0$ for all $r\in R$. But this contradicts that 
$R$ is assumed to be an extension ring of $\Z$, which, to remind the reader, 
is a ring $R$ in which $\Z$ is embedded by an injective ring homomorphism 
$\Z \hookrightarrow R$. To verify that also every integer $n \ne 0,1,-1 $ does
not have a multiplicative inverse in $\Z$, assume 
that $m \in \Z$ is one, i.e.~that $n\cdot m = m \cdot n = 1$. If $n >0$, 
then $m >0$ as well, since otherwise 
$n\cdot (-m) = -1 <0$ and $ n\cdot (-m) >0$ by monotony of multiplication
which is a contradiction. But then $m \geq 1$ and $n > 1$, hence 
$n \cdot m >1$ which contradicts the assumption that $m$ is a multiplicative inverse of $n$.
If $n<0$, then $-m$ is a multiplicative inverse of $-n$ which is strictly positive and which we already have ruled out to have a multiplicative inverse. 
So the elements of $\Z \setminus \{ 1,-1\}$ are all not 
invertible in $\Z$. 

The ring (or better field as we will later see) of rational numbers $\Q$ 
will be defined as the minimal extension of $\Z$ in which all non-zero 
integers are multiplicatively invertible. The 
construction of $\Q$ is via \emph{localization} by the set of non-zero integers
meaning by forming abstract quotients, called fractions, of integers
by non-zero ones. The process  resembles the construction of the 
Grothendieck group, but it is not the same since we do not want to invert 
the zero element of $\Z$. 
The symbol $\Q$ for the field of rational numbers goes 
back to Giuseppe Peano who introduced it 1895  after \emph{quoziente}, the 
Italian word for \emph{quotient}.

\subsec{Localization}

\begin{definition}
  A subset $S $ of a commutative ring $R$ is called \emph{multiplicative}
  if it contains $1$ and if for all $r,s \in S$ the product $rs$ 
  is in $S$ again. 
\end{definition}

Let $R$ be a commutative ring, and $S\subset R$ a multiplicative subset. 
On the cartesian product $R\times S$ we introduce an equivalence relation
as follows.  Two pairs $(p,q),(r,s)\in R\times S$ are called equivalent,
in signs $(p,q) \sim (r,s)$, if $p s t = r q  t$ 
for some $t\in S$.  Obviously, $\sim$ is reflexive and symmetric. 
To verify transitivity, assume that $(a,b) \sim (p,q)$ and $(p,q) \sim (r,s)$.
Choose $d,t \in  S$ such that $a q  d = p b  d$ and 
$p s  t = r  q  t$. Then 
\[
  a  s (q  d  t) =  a q d (s t) = pbd  (st) = 
  p s t ( b  d  ) = rqt  (bd) 
  =  r  b  ( q  d t )\ ,
\]
hence $ (a,b) \sim (r,s)$, so $\sim$ is transitive and an equivalence relation 
indeed. We denote the equivalence class of $(p,q)$ by 
\[
   \frac pq 
\]
and call it the \emph{abstract quotient} or \emph{fraction} of $p$ by $q$. 
The set of fractions $ \frac pq$ with $p\in R$, $q\in S$ is denoted by 
$S^{-1}R$ and called the \emph{localization} of $R$ by $S$. 

\begin{lemma}
  For every element $t$ of a multiplicative subset $S$ of a commutative ring $R$ 
  and every element $\frac pq \in S^{-1}R$ the fractions 
   $\frac pq$ and $\frac{pt}{qt}$ coincide. 
\end{lemma}

\begin{proof}
  This is clear since $p\, qt = pt\, q$.
\end{proof}

\begin{proposition}
\label{thm:localization-ring-multiplicative-subset}
  Let $R$ be a commutative ring and $S\subset R$ a multiplicative subset. 
  \begin{romanlist}
  \item\label{ite:localization-ring}
  The localization $S^{-1} R$ carries a ring structure with addition
  \[
    + : S^{-1} R \times S^{-1} R \to S^{-1} R, \quad 
   \Big( \frac p q , \frac r s \Big) \mapsto \frac{ps + rq}{qs} \ ,
  \]
  multiplication 
   \[
    \cdot : S^{-1} R \times S^{-1} R \to S^{-1} R, \quad 
    \Big( \frac p q , \frac r s \Big) \mapsto \frac{pq}{qs} \ ,
  \]
  zero element $0 := \frac 0 1$ and multiplicative identity $1 := \frac 11$. 
  \item 
   For every $s\in S$, the fraction $\frac 1s$ is the inverse of $\frac s1$ in $S^{-1} R$. 
  \item
   The map $R \to S^{-1} R$, $r \mapsto \frac r1$ is a ring homomorphism. 
  \item
  In case $S$ does not contain any zero divisors that is if $s r \neq 0$ for all $s\in S$ and $r\in R_{\ne 0}$,  
  then the canonical ring homomorphism $R \to S^{-1} R$ is injective. 
  Moreover in this case, two fractions $\frac pq$ and 
  $\frac rs$ are identical if and only if $ps = rq$. 
  \end{romanlist}
\end{proposition}

\begin{proof}
  \begin{adromanlist}
  \item  
  First one needs to verify that addition and multiplication are well-defined. 
  To this end let $\frac pq = \frac{p'}{q'}$ and  $\frac rs = \frac{r'}{s'}$.
  This means there are $t,u \in S$ such that 
  $p  q' t = p' q t$ and $r s' u = r' s u$. Then 
  \[
    (ps + rq) \, q' s' \, t u = pq't (s s' u) + r s' u (q q' t) =
    p'qt (ss' u) + r's u( qq't) =   (p's' + r'q') \, q s \, t u  \ ,
  \]
  so the sum of two fractions is well-defined. Next 
  \[
    pr \, q's' \, ut = (pq't)(rs'u) = (p' q t)( r' s u) = 
    p'r' \, qs \, ut \ ,
  \]
  so the product of two fractions is well-defined as well. 

  To prove associativity of addition compute
  \begin{equation*}
  \begin{split}
    \Big( \frac a b +  \frac p q \Big) + \frac r s & = 
    \frac{aq + pb}{bq}  + \frac r s =
    \frac{(aq + pb)s + rbq}{bqs} = \\
    & = \frac{a qs + (ps+rq)b}{bqs} = 
    \frac a b + \frac{ps + rq}{qs} = 
    \frac a b + \Big( \frac p q + \frac r s \Big) \ .  
  \end{split}
  \end{equation*}
  Since $R$ is a commutative ring, 
  \[
    \frac p q + \frac r s  = \frac{ps + rq}{qs} =
    \frac{rq + ps}{sq}  =  \frac r s  + \frac p q \ ,
  \]
  hence addition in $S^{-1}R$ is commutative. 
  The fraction $\frac 01$ acts as neutral element by addition:
  \[
   \frac 01 + \frac pq =  \frac{0 \cdot q + p \cdot 1}{1 \cdot q} = \frac pq \ .
  \] 
  And the additive inverse of $\frac p q$ is given by the fraction 
  $\frac{-p}{q}$\ :
  \[
   \frac p q + \frac{-p}{q} = \frac{p q + (-p)q}{q^2} = \frac{(p -p)q}{q^2}
   = \frac{0}{q^2} = \frac{0}{1} \ .
  \]
  So we have shown  that $S^{-1}R$ with addition is an abelian group. 
  Let us consider multiplication now. Multiplication in $S^{-1}R$ is obviously 
  associative since it is in $R$. More precisely, 
  \begin{equation*}
  \begin{split}
    \Big( \frac a b  \cdot  \frac p q \Big) \cdot \frac r s & = 
    \frac{ap}{bq} \cdot \frac r s = \frac{(ap)r}{(bq)s} = 
     \frac{a(pr)}{b(qs)} =  \frac ab \cdot \frac{pr}{qs} = 
    \frac a b \cdot \Big( \frac p q\cdot \frac r s \Big) \ .  
  \end{split}
  \end{equation*} 
  Similarly, multiplication in $S^{-1}R$ is commutative:
  \[
     \frac p q \cdot \frac r s = \frac{pr}{qs} = \frac{rp}{sq} = 
     \frac r s \cdot \frac p q \ .
  \]
  The element $\frac 11$ acts neutrally by multiplication, since
  \[
    \frac 11 \cdot  \frac p q = \frac{1 \cdot p}{1 \cdot q} = \frac pq \ .
  \]   
  Note that $\frac 11 = \frac tt$ for all $t\in S$ since 
  $1 \cdot t = t \cdot 1$. Using this, 
  one proves that  multiplication distributes over addition:
  \begin{equation*}
  \begin{split}
    \frac a b \cdot  \Big( \frac p q + \frac r s \Big) & =
    \frac ab \cdot  \frac{ps + rq}{qs}  = 
    \frac{a (ps + rq)}{bqs} = \frac bb \cdot \frac{a (ps + rq)}{bqs} = \\
    & = \frac{aps + arq}{bqs} = \frac{ap \, bs + ar \, bq}{bq \, bs} 
    = \frac{ap}{bq} + \frac{ar}{bs} 
    =  \frac a b \cdot   \frac p q +  \frac a b \cdot \frac r s \ .
  \end{split}
  \end{equation*} 
  We have verified that $S^{-1}R$ is a commutative ring. 
  \item
  Since for every $s \in S$ the relation 
  \[
    \lambda (s) \cdot \frac 1s = \frac s1 \cdot \frac 1s = \frac ss = 1
  \]
  holds true, $\frac 1s  $ is the inverse of $\lambda (s)$. 
  \item
  The canonical mapping 
  $\lambda : R \to S^{-1}R$, $r \mapsto \frac r1$ is a ring 
  homomorphism, since
  \[
    \frac r1 + \frac s1 = \frac{r + s}{1}, \quad  
    \frac r1 \cdot \frac s1 = \frac{rs}{1}, 
  \]
  and since $\frac 01$ and $\frac11$ are  neutral  with respect 
  to addition and multiplication, respectively. 
  \item
  Now we show that $\lambda$ is injective 
  if $S$ does not contain any zero divisors. Assume that 
  $\lambda(r)=\lambda (s)$. Then $\lambda (r -s)=0$, hence there exists 
  a $t\in S$ such that  $(r-s) t = 0$. But since $S$ does not have any zero 
  divisors $r=s$, so $\lambda$ is injective. 

  Finally consider the equation
  $\frac pq = \frac rs$. It is equivalent to $pst = rqt$ for some $t\in S$,
  hence to  $(ps-rq)t=0$. If $S$ does not have zero divisors,  the latter is 
  equivalent to $ps = rq$.  
  This finishes the proof of the proposition. 
  \end{adromanlist}
\end{proof}

\begin{remark}
  In the case where the multiplicative subset $S$ of the commutative ring $R$ 
  does not contain any zero divisors, one identifies $R$ with its image in 
  $S^{-1} R$ and denotes every element in $S^{-1} R$ of the form $\frac r1$ 
  just by $r$.
\end{remark}

\begin{theorem}\label{thm:universal-property-localization}
  Let $R$ be a commutative ring and  $S \subset R$ a multiplicative subset. Then the localization 
  $S^{-1}R$ fulfills the following universal property:
  \begin{axiomlist}[\hspace{4mm}]
  \item[\textup{({\sffamily Loc})}]
  \label{axiom:universal-property-localization}
    Let $A$ be a ring and $f : R \to A$ a ring homomorphism such that for every $s \in S$ the element $f(s)$ is 
    invertible in $A$. Then there exists a unique ring homomorphism 
    $f_{S^{-1}R} : S^{-1}R\to A$ which makes the diagram 
    \[
    \begin{tikzcd}
      R \ar[d] \ar[r,"f"] & A\\ S^{-1}R \ar[ur,"f_{S^{-1}R}",swap]
   \end{tikzcd}
   \]
    commute. 
   \end{axiomlist}
\end{theorem}

\begin{proof}
  Observe that for all $r,s\in R$ the equality
  \[
    f(s) f(r) = f(rs)= f(sr)= f(r)f(s)
  \]
  holds true. Hence $f(s)^{-1} f(r)= f(r) f(s)^{-1}$ if $s\in S$. 
  If $p,r \in R$, $q,s\in S$ and $(p,q) \sim (r,s)$, 
  choose some $t\in S$ such that $pst=rqt$. This entails 
  $f(p)f(s)f(t) = f(r)f(q)f(t)$ and, by invertibility of $f(t)$ in $A$,
  $f(q)^{-1} f(p) = f(s)^{-1} f(r)$. Hence the map  
  \[
   f_{S^{-1}R} : S^{-1} \to A, \: \frac{r}{s} \mapsto f(s)^{-1} f(r) 
  \]
  is well-defined. Moreover, it is a ring homomorphism by the following calculations:
  \begin{equation*}
  \begin{split}
    f_{S^{-1}R} (0) & = f_{S^{-1}R} \left( \frac{0}{1} \right) = f(1)^{-1} f(0) = 0 \ , \\
    f_{S^{-1}R} (1) &  = f_{S^{-1}R} \left( \frac{1}{1} \right) = f(1)^{-1} f(1) = 1 \ , \\
    f_{S^{-1}R} \left( \frac{r}{s} \right) & + f_{S^{-1}R} \left( \frac{p}{q} \right) =
    f(s)^{-1} f(r)  + f(q)^{-1} f(p) = \\ & = f(sq)^{-1} \big( f(q) f(r) + f(s) f(p) \big) = 
    f_{S^{-1}R} \left( \frac{qr + sp}{sq} \right) =  f_{S^{-1}R} \left( \frac{r}{s}+ \frac{p}{q}\right) \ ,\\ 
    f_{S^{-1}R} \left( \frac{r}{s} \right) & \cdot f_{S^{-1}R} \left( \frac{p}{q} \right) =
     f(s)^{-1} f(r) f(q)^{-1} f(p) = f(sq)^{-1} f(rp) = f_{S^{-1}R} \left(  \frac{r}{s} \cdot \frac{p}{q} \right) \ .
  \end{split}
  \end{equation*}
  This proves the existence claim. To verify uniqueness, let $\overline{f} : S^{-1}R \to A$ be another 
  ring homomorphism such that the composition $R \to S^{-1}R \overset{\overline{f}}{\to} A$ coincides  
  with $f$. Then compute for $r\in R$ and $s\in S$
  \[
    \overline{f}\left( \frac{r}{s}\right) =  \overline{f}(s)^{-1} \overline{f} (r) =
    f(s)^{-1} f(r) = f_{S^{-1}R} \left( \frac{r}{s}\right) \ .
  \]
  So $ \overline{f} = f_{S^{-1}R}$ and  $f_{S^{-1}R}$ is uniquely determined.
\end{proof}

\subsec{The field of rational numbers $\Q$}
%
By Proposition \ref{thm:ring-integers-no-nonzero-zero-divisors}, the set $ S := \nzZ$ of non-zero integers
is multiplicative. Hence the following definition makes sense.
\begin{definition}
 The \emph{field of rational numbers} $\Q$ is defined as the localization 
 $S^{-1}\Z$ of $\Z$ by the multiplicative set $S $ of non-zero integers. 
\end{definition}



\begin{thmanddef}\label{thm:rationals-field}
  The set $\Q$ together with addition $+$ and multiplication $\cdot$ as
  binary operations and the elements $0 := \frac 01$ and $1:= \frac 11$ as neutral elements 
  is a \emph{field} which means that the following axioms hold true:
  \begin{axiomlist}[Fld]
  \item \label{axiom:field-rationals-addition-abelian-monoid}
         $\Q$ together with addition $+$ and the element $0$ is an abelian group.  
  \item \label{axiom:field-rationals-multiplication-abelian-monoid-nonzero-elements-invertible} 
         $\Q$ together with multiplication $\cdot$ and the element $1$ is an abelian monoid such that 
         every element $\nzQ := \Q \setminus \{ 0 \} $ has  a multiplicative inverse.  
  \item \label{axiom:field-rationals-distributivity}
         Multiplication distributes from the left and the right over addition. 
  \item\label{axiom:field-rationals-nonequality-neutral-elements} The neutral elements $0$ and $1$ are not equal. 
  \end{axiomlist}


  Moreover, the canonical map $\Z \hookrightarrow \Q$, 
  $p \mapsto n := \frac p1$ is an injective  ring homomorphism, so $\Z$ can be identified with 
  its image $\big\{ \frac n1 \in \Q \mid n \in \Z \}$ in $\Q$. 
\end{thmanddef}

\begin{proof}
  By \Cref{thm:localization-ring-multiplicative-subset} we know that $\Q$ is a commutative ring and 
  that $\Z \hookrightarrow \Q$, $p \to \frac p1$ an injective ring homomorphism.  
  In particular this verifies Axioms \ref{axiom:field-rationals-addition-abelian-monoid} and 
  \ref{axiom:field-rationals-distributivity} and that $\Q$ with multiplication $\cdot$ and the element $1$ is
  an abelian monoid. 
  Since $0 \neq 1$ in $\Z$ (because $\N \hookrightarrow \Z$ is injective, $1 =s(0)$ and 
  $0$ is not in the image of the successor map $s : \N \to \N$), Axiom \ref{axiom:field-rationals-nonequality-neutral-elements} 
  holds true. It remains to show that every non-zero element of $\Q$ has a multiplicative inverse. 
  So let $\frac pq \in \Q$ be non-zero. Then $p$ and $q$ are both in $\nzZ$. The element
  $\frac qp$ now is the multiplicative inverse of $\frac pq$. This finishes the proof. 
\end{proof}

\begin{definition}
  A set $\fldF$ equipped with binary operations $+$ and $\cdot$ on $\fldF$ and two elements $0,1\in \fldF$ 
  is called a \emph{field} if Axioms \ref{axiom:field-rationals-addition-abelian-monoid}  to \ref{axiom:field-rationals-nonequality-neutral-elements}
  above are satisfied (after replacing $\Q$ by $\fldF$). If in addition $\leq$ is a total order relation on 
  $\fldF$ such that the monotony axioms below are satisfied as well, then $\fldF$, or more precisley
  $(\fldF, +,\cdot,0,1,\leq)$ is called an \emph{ordered field}: 
  \begin{axiomlist}[M]
  \item
  \label{axiom:monotony-addition-ordered-field} \textup{Monotony of addition}\\
   For all $a,b\in \fldF$ and $c\in \fldF$ the relation $a < b$ implies 
   $a + c < b + c$.  
  \item
   \label{axiom:monotony-multiplication-ordered-field}  \textup{Monotony of multiplication} \\
   For all  $a,b\in \fldF$ and $c\in \fldF_{> 0} := \{x \in \fldF \mid x > 0\} $ the relation $a < b$ implies $a \cdot c < b \cdot c$.
  \end{axiomlist}
\end{definition}

\begin{definition}
  A rational number $\frac pq \in \Q$ is called \emph{less or equal} than a rational number $\frac rs \in \Q$, in signs $\frac pq \leq \frac rs$,
  if the integer $ (rq - ps) qs $ lies in $\N$.  
\end{definition}

\begin{theorem}
  The field $\Q$ together with the binary relation $\leq$ is an ordered field. Moreover, the canonical embedding
  $\Z \hookrightarrow \Q$, $p \mapsto \frac p1$ is order preserving which means that $p < q$ for $p,q\in \Z$ implies
  $\frac p1 < \frac q1$. 
\end{theorem}
\begin{proof}
  Obviously, $\leq$ is reflexive, since for $\frac pq \in \Q$ the integer $ (pq - pq) q^2 $ vanishes. 

  If $\frac pq \leq \frac rs $ and $\frac rs \leq \frac pq$, then both $ (rq - ps) qs $ and its additive inverse
  $ (ps - rq) qs $ are elements of $\N$, hence $ (rq - ps) qs =0$ which implies that 
  $\frac rs - \frac pq = \frac{rq - ps}{qs} = \frac{ (rq - ps)qs}{(qs)^2} = 0$. So $\leq $ is antisymmetric. 

  If $\frac ab \leq \frac pq$ and $\frac pq   \leq \frac rs$, then 
  $ (pb - aq) bq \geq 0 $ and $ (rq -ps ) qs \geq 0 $. Hence 
  $ \big( (rb - as )bs \big) qq = ( rbq - aqs )bqs = (rq -ps ) qs b^2 + (pb - aq) bq  s^2 > 0$, since
  $b^2 > 0$ and $s^2>0$. Since $q^2 >0$, $(rb - as )bs$ follows by monotony of multiplication in $\Z$. 
  This proves that $\leq$  is transitive. So we have shown that $\leq$ is an order relation on $\Q$.
  

  Since for two rational numbers $\frac pq$ and $\frac rs$ the integer  $ (rq -ps ) qs$ is either positive, zero, or negative, 
  the trichotomy law holds true, and $\leq$ is a total order. 

  Note that the relation $\frac rs > 0 $ is equivalent to $ rs >0$. Hence, if $\frac ab < \frac pq$ and $\frac rs >0$, 
  then $\frac{ar}{bs} < \frac{pr}{qs}$ since $(prbs - arqs)bsqs = \big( (pb-aq)bq\big) (rs) s^2  > 0$. Therefore, monotony of multiplication holds.
  Now let $\frac rs \in \Q$ be arbitrary and $\frac ab < \frac pq$ as before. 
  Then 
  \[
     \frac ab + \frac rs = \frac{as + rb}{sb}\quad \text{and} \quad  \frac pq + \frac rs = \frac{ps + rq}{sq} \ .
  \]
  Now compute
  \[
    \big( (ps + rq) sb - (as + rb) (sq) \big) sbsq = \big( (pb -aq) bq \big) s^4 > 0 \ .
  \]
  Hence $\frac ab + \frac rs < \frac pq + \frac rs$, and monotony of addition is finally proved as well.  

  Finally assume $p < q$ for integers $p,q$. Then $(q\cdot 1 - p \cdot 1) \cdot 1^2 = p - q > 0$,
  hence $\frac p1 \leq \frac q1$. But $p \neq q$ since the ring homomorphism $\Z\hookrightarrow \Q$ is 
  injective. So one even has  $\frac p1 < \frac q1$ as claimed.
\end{proof}

\begin{theorem}\label{thm:field-rational-numbers-archimedean-ordered}
  The field of rational numbers $\Q$ is an archimedean ordered field that is for every pair 
  of rational numbers $\frac pq$, $\frac rs$ with $\frac rs > 0$ there exists a natural number 
  $n \in \N$ such that $\frac pq < n \, \frac rs $. 
\end{theorem}

\begin{proof}
  By \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:inverse-preserving-positivity}
  and since $\frac rs > 0$, the claim is equivalent to the existence of a natural number 
  $n \in \N$ such that $\frac{ps}{qr} < n$. So it suffices to prove that for every rational 
  $\frac pq$ there exists  $n \in \N$ such that $ \frac pq < n$. Let us show this. 
 After possibly multiplying both $p$ and $q$ by $-1$ we can assume that $ q \in \gzN$. If 
  $p \leq 0$, put $n=1$ and observe $ p \leq 0 < q = n \cdot q$. If $p>0$, then $p \in \N$, so  
  by the archimedean property for natural numbers \ref{thm:archimedean-property-natural-numbers} 
  there exists $n \in \N$ such that $ p < n \cdot q $. Now observe that $0 < \frac 1q$ because 
  $(1 \cdot 1 - 0 \cdot q) \cdot 1 \cdot q = q > 0$. 
  By monotony of multiplication, we can now conclude   
  from $ p < n \cdot q $ that $\frac pq < n$.  This proves the claim. 
\end{proof}

\para 
  An important observation for the field of rational numbers is that not all equations of the 
  form $x^2 + a =0$ for given $a \in \Q$ have  a solution $x \in \Q$. 
  
  \begin{proposition}
  \label{thm:non-existence-rational-root-negative-one-prime-number}
    Let $n\in \N $ be a prime number. Then the equation
    $x^2 - n =0$ does not have  a rational solution.
  \end{proposition}
  
  \begin{proof}
    Assume that there is a rational number $x $ such that $x^2 =n$. 
    After possibly passing to $-x$ we can assume that there exist $p\in \N$ and $q \in \gzN$
    such that $x  = \frac pq$. We can even assume that $q$ is the least positive natural number for which 
    there exist  a $p\in \N$ such that $x =\frac pq$. Then $ n \cdot q^2 = p^2$. Since $n$ is prime, 
    Euclid's Lemma \ref{thm:euclids-lemma} implies that $n$ is a divisor of $p$. Write $ p = n \cdot r$ 
    for some $r\in \N$. 
    Then $ q^2 = n \cdot r^2$, which entails  by Euclid's Lemma again that $n$ is a divisor of $q$. Hence
    $ q = n \cdot s$ for some $s \in \gzN$. Observe that $s$ has to be smaller than $q$. 
    Now $x = \frac pq = \frac rs$ which contradicts the minimality of $q$.  
  \end{proof}

\subsec{Ordered fields}
\para
In the following we introduce new concepts for ordered fields and will derive properties which hold in 
any ordered field, so in particular in $\Q$.  

Let us recall some notation and assume that $(\fldF,+,\cdot,0,1, \leq)$ is an ordered field.
Then $\fldF$ (or better $(\fldF,+,\cdot,0,1, \leq)$) is in particular an ordered integral domain, so
the sets  $\fldF_{>0}$ of \emph{positive} elements and $\fldF_{<0}$ of \emph{negative} elements are 
defined as $\{ x \in \fldF \mid 0 < x \}$ and  $\{ x \in \fldF \mid x < 0 \}$, respectively, cf.~\Cref{def:ordered-commutative-ring}. The sets $\fldF_{\geq 0} := \{ x \in \fldF \mid 0 \leq x \}$ and 
$\fldF_{\leq 0} := \{ x \in \fldF \mid x \leq 0 \}$ are the the sets of \emph{non-negative}
and \emph{non-positive} elements of  $\fldF$, respectively. 

\begin{propanddef}
  Let $(\fldF,+,\cdot,0,1, \leq)$ be an ordered field. Then the set $\fldF^+:= \fldF_{\geq 0}$  of  
  non-negative elements is a \emph{positive cone} that is it satisfies the following axioms:
  \begin{axiomlist}[PC]
  \item\label{ite:positive-cone-stability-addition}\hspace{2mm}
    For all $x,y \in \fldF^+$ the sum $x +y$ lies in $ \fldF^+$.
  \item\label{ite:positive-cone-stability-multiplication}\hspace{2mm}
    For all $x,y \in \fldF^+$ the product $x \cdot y$ lies in $\fldF^+$.
  \item\label{ite:positive-cone-containing-squares}\hspace{2mm} 
    The square $x^2$ is an element of $\fldF^+$ for every $x\in \fldF$. 
  \item\label{ite:positive-cone-not-containing-negative-one}\hspace{2mm}
    The element $-1$ does not lie in $\fldF^+$.
  \item\label{ite:union-positive-cone-negative-cone}\hspace{2mm} 
    The field $\fldF$ is the union of $ \fldF^+$ and $\fldF^- := -\fldF^+$. 
  \end{axiomlist}
\end{propanddef}

\begin{proof}
    \ref{ite:positive-cone-stability-addition} follows by monotony of addition and transitivity:
    $x + y \geq x \geq 0$,  
    \ref{ite:positive-cone-stability-multiplication} by monotony of multiplication:
    $ x \cdot y \geq x \cdot 0 = 0$. 
    \ref{ite:positive-cone-containing-squares} is a consequence of 
    \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:square-positive}. 
    Adding $-1$ to the inequality 
    $0 < 1$ gives $ -1 < 0$ which entails \ref{ite:positive-cone-not-containing-negative-one}. 
    Next observe that by \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:antimonotony-negative}
    $\fldF^- = \fldF_{\leq 0}$. \ref{ite:union-positive-cone-negative-cone} then follows by the trichotomy law
    for $\leq$. 
\end{proof}

\begin{remark}
 The proof of the proposition also shows $\fldF^- = \fldF_{\leq 0}$. 
\end{remark}

\begin{corollary}
  In an ordered field $\fldF$ the equation $x^2 = 1$ does not have a solution.
\end{corollary}

\begin{proof}
  This is an immediate consequence of \ref{ite:positive-cone-containing-squares} and 
  \ref{ite:positive-cone-not-containing-negative-one}.
\end{proof}

\begin{proposition}\label{thm:canoncial-embedding-integers-ordered-field}
  Let $(\fldF,+,\cdot,0,1,\leq)$ be an ordered field. Then the map 
  \[
    \Z \to \fldF, \quad p \mapsto p \cdot 1 := 
    \begin{cases}
       \sum\limits_{i=1}^p 1 & \text{if } p \in \gzZ,\\
       0 &  \text{if } p = 0, \\
       - \sum\limits_{i=1}^{-p} 1  & \text{if } p \in  \lzZ,
    \end{cases}
  \] 
  is a strictly order preserving embedding of rings.  It is the only ring homomorphism 
  from $\Z$ to $\fldF$. 
\end{proposition}

\begin{proof}
  By Proposition \ref{thm:power-operation-group-homomorphism}, the map $\iota : \Z \to \fldF$, $p \mapsto p \cdot 1$
  is a group homomorphism with respect to the additive group structures on $\Z$ and $\fldF$. 
  Observe that by definition $\iota (1)=1$. 
  Next let us show by induction on $n$ that $\iota (  np ) = \iota (n) \iota(p)$ for all $n\in \N$ and $p\in \Z$.  
  For $n =0,1$ the claim is trivial. Assume that it holds for some $n>0$. Then
  \[
   \iota ((n+1)p) = \iota (np + p) = \iota(n)\iota(p) + \iota (p) = (\iota (n) + 1) \iota (p ) = 
   \iota(n+1) \iota(p) \ ,
  \]
  which shows that the claim holds for $n+1$ as well. Moreover, 
  \[
    \iota ((-n)p) =  - \iota (n p) = - \iota(n) \iota(p) = \iota(-n) \iota(p) \ .
  \]
  Hence  $\iota $ is a ring homomorphism. 
  Now let $p,q \in \Z$ and assume $q < p$. Then $n = p-q \in \gzN$ and 
  $p \cdot1 - q \cdot 1 = n \cdot 1 = \sum_{i=1}^n 1 >0$, where the latter inequality follows by induction on $n$. 
  Therefore  the map $\Z \to \fldF$, $ p \mapsto p \cdot 1$ is strictly order preserving and in particular injective. 
  If $\mu : \Z \to \fldF$ is another ring homomorphism, then for $n \in \N$
  \[
    \mu (n) = \mu \left( \sum\limits_{i=1}^n 1  \right) =   \sum\limits_{i=1}^n \mu (1) = \iota (n) 
    \quad\text{and} \quad 
    \mu (-n) = - \mu \left( \sum\limits_{i=1}^n 1  \right) =  - \sum\limits_{i=1}^n \mu (1) = \iota (-n) 
  \]
  since $\mu (1)=1$. Hence $\iota$ and $\mu$ coincide. 
\end{proof}

\para 
  Since by the preceding result the set of integers is a subset of any ordered field 
  $(\fldF,+,\cdot,0,1,\leq)$, one can ask the question whether $\N$ or $\Z$ are unbounded within that 
  field. It will turn out that this is not always so. 

\begin{definition}
  An ordered field $(\fldF,+,\cdot,0,1,\leq)$ is said to be \emph{archimedean ordered} 
  if for every pair of elements $x,y \in \fldF$  with $x > 0$ there 
  exists a natural number $n \in \N$ such that $y < n  \cdot x$. 
\end{definition}

\begin{example}
  The field of rational numbers is archimedean ordered by 
  \Cref{thm:field-rational-numbers-archimedean-ordered}. 
\end{example}

\para The field of rational numbers is the smallest archimedean field and contained
  in any other archimedean ordered field by the following result. 
\begin{proposition}
  Every archimedean ordered field $\fldF$ contains $\Q$ as subfield 
  in a canonical way. More precisely, there exists a unique strictly order preserving ring homomorphism 
  $ \Q \hookrightarrow \fldF$.    
\end{proposition}
  
\begin{proof}
  By \Cref{thm:canoncial-embedding-integers-ordered-field} there exists a uniquely determined 
  strictly order preserving ring homomorphism $\Z \to \fldF$. By the universal property of localization, 
  see \Cref{thm:universal-property-localization}, this ring homomorphism extends in a unique way to 
  a ring homomorphism $\Q \to \fldF$. Let us show that this ring homomorphism, which we denote by $\iota$, is order preserving. 
  Assume that $\frac{p}{q}$ and $\frac{r}{s}$ are rational numbers and that  $\frac{p}{q} < \frac{r}{s}$.
  After possibly passing to  $\frac{-p}{-q}$ respectively $\frac{-r}{-s}$ 
  we can assume that $q,s \in \gzN$. 
  Then $n := (rq-ps)qs \in \gzN$ and $(\iota (r) \iota (q) - \iota(p) \iota (s)) \iota (q s) = \iota (n) > 0$
  by \Cref{thm:canoncial-embedding-integers-ordered-field}. 
  Since $\iota (q s) = \iota(q) \iota(s) > 0$,  $\iota (q s)^{-1}$ is positive by 
  \Cref{thm:properties-order-ordered-integral-domain} \ref{ite:inverse-preserving-positivity}. Hence
  \[
    \iota\left( \frac{r}{s} \right) - \iota\left( \frac{p}{q} \right) = \iota(r) \iota(s)^{-1} - \iota(p)\iota(q)^{-1} >0 
  \]
  and $\iota$ is strictly order preserving.   
\end{proof}

\begin{remark}
  By the proposition one can identify for every ordered field $\fldF$ the field of rationals with its image 
  under the canonical embedding $ \Q \hookrightarrow \fldF$. We will follow that convention in this work
  and will call the elements in the image of the embedding  $ \Q \hookrightarrow \fldF$
  the \emph{rational elements} of $\fldF$.  
\end{remark}

\begin{proposition}
  For an ordered field $(\fldF,+,\cdot,0,1,\leq)$ the following properties are equivalent:
  \begin{romanlist}
  \item\label{ite:archimedean-property}
     The field $\fldF$ is archimedean ordered.  
  \item\label{ite:weak-archimedean-property}
     For every $y \in \fldF$ there exists $n\in \N$ such that $y < n$.  
  \item \label{ite:squeezing-rational-between-elements}
     For all $x,y\in \fldF$ with $x < y$ there exists a rational element $r$ such that   
     $x < r < y$. 
  \item\label{ite:sqeezing-between-naturals}
     For all $x \in \fldF$ with $x\geq 0$ there exists a unique $n\in \N$ such that $n \leq  x < n+1$.  
  \end{romanlist}
\end{proposition}

\begin{proof}
  First observe that \ref{ite:weak-archimedean-property} implies \ref{ite:archimedean-property} since 
  for $x,y \in \fldF$ with $x > 0 $  \ref{ite:weak-archimedean-property} entails the exsistence 
  of a natural $n$ with $\frac{y}{x} < n$. By positivity of $n$ one obtains $y < n\cdot x$, hence  \ref{ite:archimedean-property}.
  The converse implication is trivial, so \ref{ite:archimedean-property} and \ref{ite:weak-archimedean-property}
  are equivalent.

  Assume that $\fldF$ is archimedean ordered. Let us show that then \ref{ite:sqeezing-between-naturals} 
  holds true. So let $x \geq 0$. The set of natural numbers $\leq x$ contains at least $0$ and is finite 
  by the archimedean property. Hence it has a maximal element. Denote that element by $n$. Then $x < n+1$ 
  by maximality of $n$. To verify uniqueness let $m$ be another natural number with $m \leq x < m+1$.
  Then $m \leq n$ by maximality of $n$. One concludes $n \leq m$ from $n \leq x < m+1$. Hence $n=m$ and
  \ref{ite:sqeezing-between-naturals} is proved. Note that \ref{ite:sqeezing-between-naturals} 
  implies \ref{ite:weak-archimedean-property}. So \ref{ite:archimedean-property} and \ref{ite:sqeezing-between-naturals} 
  are equivalent. 

  Now assume $\fldF$ is archimedean and that $x,y$ are elements of $\fldF$ with $x < y$.
  Since $y-x >0$ there exists a $q \in \gzN$ with $1 < q\cdot ( y-x ) $. Then choose $p \in \N$ such that
  $p \leq qx < p+1$. One concludes $qx < p+ 1\leq qx +1 < qy$, hence $x < \frac pq < y$. This proves
  \ref{ite:squeezing-rational-between-elements}. If conversely  \ref{ite:squeezing-rational-between-elements} 
  holds true and $x >0$ then there exist $p, q \in \gzN$  with $x < \frac pq < x+1$. Therefore $x \leq qx < p$ 
  which entails \ref{ite:weak-archimedean-property}. 
\end{proof}


\begin{definition}
  Let $(\fldF,+,\cdot,0,1,\leq)$ be an ordered field. The \emph{absolute value function} 
  on $\fldF$ is the following map:
  \[
   | \cdot | : \fldF \to \fldF , \quad x \mapsto 
    \begin{cases}
       x & \text{if } x \in \fldF_{>0} = \fldF^+\setminus \{ 0 \} ,\\
       0 &  \text{if } x = 0, \\
       - x  & \text{if } x \in  \fldF_{<0} = \fldF^- \setminus \{ 0 \}  \ . 
    \end{cases}
  \]
  One calls $|x|$ the \emph{absolute value} of $x \in \fldF$. 
\end{definition}

\begin{remark}
  Since an ordered field is in particular an ordered integral domain, 
  all properties of \Cref{thm:properties-order-ordered-integral-domain}
  and \Cref{thm:properties-absolute-value-ordered-commutative-ring}
  hold true for the absolute value function of an ordered field.  
\end{remark}



\subsec{The order topology}

\para Let $(X,\leq)$ be a totally ordered set. \emph{Intervals} in $X$ are subsets $I \subset X$ 
  which contain with every pair of elements $x < y$ also every element $z$ between them which means that 
  for all $x,y \in I$ with $x<y$ and all $z \in X$ with $ x \leq z \leq y$ the relation $z \in I$ holds true. 
  The following subsets are clearly intervals, where $x,y$ denote arbitrary elements of $X$ and $\pm\infty$ are two symbols assumed not to stand for elements of $X$:
\begin{equation*}
  \begin{split}
    \openint{x,y} & :=  \openint{x,y}_X := \big\{ z \in X   \mid x < z < y \big\} \ , \\
    \leftclosedint{x,y} & :=  \leftclosedint{x,y}_X :=  \big\{ z \in X   \mid x \leq z < y \big\} \ , \\
    \rightclosedint{x,y} & :=  \rightclosedint{x,y}_X :=  \big\{ z \in X   \mid x < z \leq y \big\} \ ,\\
    \closedint{x,y} & :=  \closedint{x,y}_X :=  \big\{ z \in X   \mid x \leq z \leq y \big\} \ , \\ 
    \openint{x,\infty} & :=  \openint{x,\infty}_X := \big\{ z \in X   \mid x < z  \big\} \ , \\
    \openint{- \infty ,y} & :=  \openint{-\infty ,y}_X := \big\{ z \in X   \mid  z < y \big\} \ , \\ 
   \openint{- \infty , \infty} & :=  \openint{-\infty ,\infty }_X := X \ , \\
    \leftclosedint{x,\infty} & :=  \leftclosedint{x,\infty}_X := \big\{ z \in X   \mid x \leq z  \big\} \ , \\
    \rightclosedint{- \infty ,y} & :=  \rightclosedint{-\infty ,y}_X := \big\{ z \in X   \mid  z \leq y \big\} \ . \\
  \end{split}
  \end{equation*}
One calls intervals of the form $ \openint{x,y}_X$, $\openint{x,\infty}_X$, 
 $\openint{- \infty ,y}$, or $\openint{-\infty ,\infty }_X$ \emph{open intervals} in $X$, of the form $\closedint{x,y}_X$, $\leftclosedint{x,\infty}_X$, or $\rightclosedint{-\infty ,y}_X$ 
\emph{closed intervals}, and of the form $\leftclosedint{x,y}_X, \rightclosedint{x,y}_X$ \emph{half-open intervals}. 
\begin{remark}
  When the context makes it clear which underlying set $X$ is meant, we will usually use the notation
  $\openint{x,y}$ instead of  $\openint{x,y}_X$,  $\leftclosedint{x,y}$ instead of $\leftclosedint{x,y}_X $ and so on. 
\end{remark}
\begin{definition}
  Let $(X, \leq)$ be a totally ordered set with more than one elements. 
  The \emph{order topology}  $\topology_{(X, \leq)}$  on $(X,\leq)$ (or just $X$) is 
  then defined as the set of all subsets $O\subset X$ such that for each $x\in O$ one of the following holds:
  \begin{romanlist}
  \item
    there exist $a,b \in X$ with $x \in \openint{a,b} \subset O$,
  \item
    the element $x$ is a minimum of $X$ and there exists  $b\in X$ such that $\leftclosedint{x,b}\subset O$,
  \item
    the element $x$ is a maximum of $X$ and there exists  $a\in X$ such that $\rightclosedint{a,x}\subset O$,
  \end{romanlist}
\end{definition}

\begin{thmanddef}
  The set $\topology = \topology_{(X, \leq)}$  associated to a totally ordered set $(X,\leq)$ is a \emph{topology} on $X$ which means that 
  the following axioms hold true:
  \begin{axiomlist}[Top]
\setcounter{enumi}{-1}
\item\label{axiom:openness-full-empty-set-open-order-topology}
      The sets  $X$ and $\emptyset$ are both elements of $\topology$.
\item\label{axiom:openness-union-open-sets-order-topology}
      The union of any collection of elements of $\topology$ is again in $\topology$ that means if
      $(U_i)_{i\in I}$ is a family of elements $U_i\in \topology$, then
      $\bigcup_{i\in I} U_i \in \topology$.
\item \label{axiom:openness-finite-intersection-open-sets-order-topology}
      The intersection of finitely many elements of $\topology$ is again in $\topology$ that is 
      for every natural $n$ and $U_1, \ldots , U_n  \in \topology$ the set
      $ \bigcap_{i = 1}^n U_i$ lies in $\topology$.
\end{axiomlist}
\end{thmanddef}

\begin{proof}
  The emptyset is an element of $\topology$ by definition. Let $x \in X$. If $x$ is neither a minimum nor a maximum, 
  then there exist $a,b\in X$ with $a< x <b$ or in other words $x \in \openint{a,b}$. If $x$ is a minimum of $X$, then 
  there exists a $ b > x$. Hence  $x \in \leftclosedint{x,b}$. Similarly, if $x$ is a maximum, there exists an $a\in X$ such that 
  $x \in \rightclosedint{a,b}$. So $X \in \topology$ and \ref{axiom:openness-full-empty-set-open-order-topology} is proved.

  Next let $x \in \bigcup_{i\in I} U_i $, where each $U_i\in \topology$, $i\in I$. Choose $j \in I$ such that $x \in U_j$. 
  Then either $ I = \openint{a,b} \in U_j$ for some $a,b$ with $a < x <b$, $ I = \leftclosedint{x,b}  \in U_j$
  for some $b > x$, or  $ I = \rightclosedint{a,x}  \in U_j$   for some $a < x$. In each case one has 
  $I \subset \bigcup_{i\in I} U_i$ which proves \ref{axiom:openness-union-open-sets-order-topology}.

  Finally let $x \in \bigcap_{i = 1}^n U_i$, where $U_1, \ldots , U_n  \in \topology$. Assume first that $x$ is neither a minimal nor a maximal 
  element of $X$. Then one can find $a_1,\ldots , a_n, b_1,\ldots ,b_n \in X$ such that $a_i < x < b_i$ 
  and $\openint{a_i,b_i} \subset U_i$  for $i=1,\ldots,n$. Let $a$ be the maximum of the $a_i$ and $b$ the minimum of the $b_i$.
  Then $ a < x <b$ and $\openint{a,b} \subset \bigcap_{i = 1}^n U_i$.
  Next assume $x$ to be a minimum of $X$. Then there are $b_1,\ldots ,b_n \in X$ such that $\leftclosedint{x,b_i} \subset U_i$ for
  $i=1,\ldots,n$. As before let $b$ denote the minimum of the $b_i$. Then  $\leftclosedint{x,b} \subset  \bigcap_{i = 1}^n U_i$. 
  The final case where $x$ is a maximum of $X$ works analogously. Choose $ a_1,\ldots , a_n \in X$ such that 
  $\rightclosedint{a_i,x}\subset U_i$  for  $i=1,\ldots,n$ and let $a$ be the maximum of the $a_i$. 
  Then $ \rightclosedint{a,x} \subset  \bigcap_{i = 1}^n U_i$. 
  So \ref{axiom:openness-finite-intersection-open-sets-order-topology} holds true as well. 
\end{proof}

\begin{remarks}
  \begin{letterlist}
  \item
  A set $X$ together with a subset $\topology \subset \powerset{X}$  such that the above axioms 
  \ref{axiom:openness-full-empty-set-open-order-topology} to \ref{axiom:openness-finite-intersection-open-sets-order-topology}
  hold true is called a \emph{topological space}.
  We sometimes denote such a topological space as a pair $(X,\topology)$.
  Subsets of $X$ which are elements of the topology $\topology$ are called 
  \emph{open}, subsets of $X$ whose complement in $X$ is open are called \emph{closed}. 
  From now on we will use this language even though the class of topological spaces will be studied in more detail 
  only later in  Chapter \ref{chpt:general-topology}.  
  \item
  For every set $X$ the power set $\powerset{X}$ and the set $\{ \emptyset , X\}$ are topologies on $X$ 
  called the \emph{finest} and the \emph{coarsest} topology on $X$, respectively. 
  \end{letterlist}
\end{remarks}

\begin{definition}
  Let $x \in X$ be a point of an ordered set $(X,\leq)$. A subset 
  $V \subset \fldF$ is called a \emph{neighborhood} of $x$, if there 
  exists an open set $U \in \topology_{(X,\leq)}$ such that $x \in U \subset V$.  
\end{definition}
