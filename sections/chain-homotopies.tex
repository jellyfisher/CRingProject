% Copyright 2016 Akhil Mathew, licensed under GNU FDL v1.3 
% main author: 
%   Akhil Mathew 
% 
\section{Chain Homotopies}\label{sec:chain-homotopies}

\para 
In general, two maps of complexes $C_\bullet \rightrightarrows D_\bullet$ need not be
equal to induce the same morphisms in homology. It is thus of interest to
determine conditions when they do. One important condition is given by chain
homotopy: chain homotopic maps are indistinguishable in homology. In algebraic
topology, this fact is used to show that singular homology is a homotopy
invariant. We will find it useful in showing that the construction (to be given later) 
of a projective resolution is essentially unique.

As before, we will understand all of the following constructions to be performed 
within the category $R\text{-}\category{Mod}$ of left modules over a fixed ring $R$,
unless stated differently.
  
\begin{definition} 
  Let $C_\bullet, D_\bullet$ be chain complexes with differentials $\partial^C$ and 
  $\partial^D$, respectively. A chain homotopy between two chain maps
  $f,g: C_\bullet \rightarrow D_\bullet$ is a sequence of homomorphism
  $h_k :C_k \rightarrow D_{k+1} $, $k\in \Z$ satisfying  
  \[
    f_k - g_k =\partial^D_{k+1} h_k +  h_{k-1} \partial^C_k \quad \text{for all } k \in \Z \ .
  \] 
  Again, often notation is abused and the condition is written 
  $f-g= \partial h + h \partial$.

  Dually, if $C^\bullet$ and $D^\bullet$ are two cochain complexes with respective 
  differentials $d_C$ and $d_D$, then a chain homotopy between two morphisms
  of cochain complexes $f,g: C^bullet \rightarrow D^bullet$ is a sequence of 
  homomorphisms $h^k :C^k \rightarrow D^{k-1} $, $k\in \Z$ satisfying     
  \[
    f^k - g^k =d_D^{k-1} h^k +  h^{k+1} d_C^k \quad \text{for all } k \in \Z \ .
  \] 
  or shortly  $f-g= d h + h d$.
\end{definition}

\begin{proposition} 
  If two morphisms of chain complexes $f,g: C_\bullet \to D_\bullet$ are chain homotopic, 
  they are taken to the same induced map after applying the homology functor.
  Likewise, two chain homotopic morphisms of cochain complexes $f,g: C_\bullet \to D_\bullet$
  induce the same map in cohomology. 
\end{proposition}

\begin{proof} 
Write $\left\{d_i\right\}$ for the various differentials (in both complexes).
Let $m\in Z_i(C)$, the group of $i$-cycles. 
Suppose there is a chain homotopy $h$ between $f,g$ (that is, a set of
morphisms $C_i \to D_{i-1}$).
Then
$$f^i(m)-g^i(m)= h^{i+1}\circ d^i(m) + d^{i-1}\circ h^i(m)= d^{i-1}\circ H^i(m) \in \Im(d^{i-1})$$
which is zero in the cohomology $H^i(D)$. 
\end{proof}


\begin{corollary} If two chain complexes are chain homotopically equivalent
(there are maps $f: C_*\rightarrow D_*$ and $g:D_*\rightarrow
C_*$ such that both $fg$ and $gf$ are chain homotopic to the
identity), they have isomorphic homology.
\end{corollary}
\begin{proof} 
Clear.
\end{proof} 

\begin{example}  Not every quasi-isomorphism is a homotopy equivalence.  Consider the complex
$$\dots \rightarrow 0\rightarrow\mathbb{Z}/{\cdot 2}\rightarrow \mathbb{Z}\rightarrow 0\rightarrow 0\rightarrow\dots$$
so $H^0=\mathbb{Z}/2\mathbb{Z}$ and all cohomologies are 0.  We have a quasi-isomorphism from the above complex to the complex
$$\dots \rightarrow 0\rightarrow 0 \rightarrow \mathbb{Z}/2\mathbb{Z}\rightarrow 0\rightarrow 0\rightarrow\dots$$
but no inverse can be defined (no map from $\mathbb{Z}/2\mathbb{Z}\rightarrow \mathbb{Z}$).
\end{example}


\begin{proposition} Additive functors preserve chain homotopies
\end{proposition}
\begin{proof} Since an additive functor $F$ is a homomorphism on $Hom(-,-)$,
the chain homotopy condition will be preserved; in
particular, if $t$ is a chain homotopy, then $F(t)$ is a chain
homotopy.
\end{proof}

In more sophisticated homological theory, one often makes the
definition of the ``homotopy category of chain complexes.''
\begin{definition} The homotopy category of chain complexes is
the category $hKom(R)$ where objects are chain complexes of
$R$-modules and morphisms are chain maps modulo chain homotopy.
\end{definition}

