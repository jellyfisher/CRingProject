% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{Arithmetic in $\Z$}
\label{sec:arithmetic-z}
%
\subsec{The fundamental theorem of arithmetic}
%
\begin{theorem}[The Division Theorem]
\label{thm:division-theorem}
  Let $a,b$ be integers with $b \neq 0$. Then there exist uniquely determined 
  $q,r \in \Z$ such that $a = q b + r$ and $0 \leq r < |b|$. 
\end{theorem}

\begin{proof}
  Consider the set $M = \{ m \in \N \mid \exists q \in \Z : \:  m = a - qb \}$.
  This set is non-empty, since by the archimedean property for $\Z$ there exists
  a natural number $n$ such that $n |b| > -a$.  
  If $b>0$ put $q = -n$ otherwise let $q = n$. In either case 
  $a - qb = a + n |b| >0$, hence $a - qb \in M$. 
  Since $\N$ is well-ordered, the set $M$ has a least element  which we 
  denote by $r$. Let $q\in \Z$ such that $r = a - qb$. 
  Assume that $r \geq |b|$. Then 
  $s = a - (q+\sgn(b))b = r - \sgn(b) b = r -|b| \in \N$, where   
  $\sgn(b)$ is defined to be equal to $1$ if $b>0$ and to be $-1$ else. 
  Hence $s \in M$ and $s<r$ which contradicts the minimality of $r$. 
  Therefore $r <|b|$. 

  Now assume that $q',r'$ are a second pair of integers such that 
  $a = q' b + r'$ and $0 \leq r' < |b|$. Then 
  $\big| (q-q')b \big| = |r'-r| < |b|$ and 
  $\big| (q-q')b \big| = |q-q'| \cdot |b|$. This can only be possible if 
  $|q -q'| =0$ which entails $q' =q$ and $r'=r$. The proof is finished.
\end{proof}

\begin{definition}
  If $r,s$ are integers or more generally elements of an integral domain $R$,
  then $r$ is said to \emph{divide} $s$ or that $r$ is a \emph{divisor} of $s$ 
  if there exists some element $q$ of $\Z$ respectively $R$ such that
  $q \cdot r = s$. One also says in this situation that $s$ is a \emph{multiple}
  of $r$ and denotes it by writing $r \mid s$. If $r$ does not divide $s$, 
  one denotes this by $r \nmid  s$. 
\end{definition}

\begin{lemma}\label{thm:divisor-bounded-number}
  If $r$ is a divisor of the non-zero integer $s$, then $|r|\leq |s|$. 
\end{lemma}

\begin{proof}
  By definition there exists an integer $q$ such that $q\cdot r=s$.
  Then $q$ must be non-zero and $|q|\cdot |r|=|s|$. Hence $|q| \geq 1$ and 
  $|r|\leq |s|$. 
\end{proof}

\para Every natural number larger than $1$ has at least two positive divisors,
namely $1$ and the number itself. The natural numbers $n >1$ for which these two divisors
are all the divisors of $n$ have a particular name, they are called \emph{prime numbers}. 

\begin{definition}
  Let $r_1,\ldots,r_k$  with $k\in \gzN $ be integers. A natural number $d$ is called 
  \emph{greatest common divisor} of $r_1,\ldots,r_k$ if $d$ is a divisor 
  of all the $r_j$ and if any natural number which is a divisor of all the $r_j$ is a divisor of $d$.
  If $1$ is a greatest common divisor of $r_1,\ldots,r_k$, then one calls $r_1,\ldots,r_k$
  \emph{coprime} or \emph{relatively prime}.
\end{definition}
 
\begin{propanddef}\label{thm:existence-uniqueness-greatest-common-divisor}
  The greatest common divisor of integers $r_1,\ldots,r_k$ exists and is uniquely 
  determined. It is denoted $\gcd (r_1,\ldots,r_k)$.
\end{propanddef}

\begin{proof}
  First assume that $r_1=\ldots = r_k=0$. Then any natural number is a divisor of each of the $r_j$. 
  In particular $0$ is a divisor of all the $r_j$ and is divided by all natural numbers. 
  Since $0$ is the only natural number with that property $0$ is a greatest common divisor 
  of $r_1,\ldots,r_k$ and it is the only one. 

  Now assume that at least one of the $r_j$ is non-zero. 
  Let us first prove uniqueness in this situation and assume that $d$ and $d'$ are greatest common divisors of 
  $r_1,\ldots,r_k$. Since $0$ is not a divisor of the non-zero elements of the $r_j$ both $d$ and $d'$ have to be
  positive. By definition of greatest common divisors $d$ divides $d'$ and vice versa. Hence there are $n,m \in \gzN$
  such that $d =nd'$ and $d'=nd$. But then $d=nmd$ which implies $nm = 1$ since $d$ is non-zero. 
  This means that $n=m=1$ and $d=d'$.

  Next we prove existence. Let $L$ be the set of all positive integers which can be written in the form 
  $x_1 r_1 + \ldots + x_kr_k$ for integers $x_1,\ldots,x_k$. The set $L$ is non-empty since it contains
  $r_1^2 + \ldots + r_k^2$. Let $d$ be its smallest element. Clearly, if $c$ divides all the $r_j$, then $c$ divides 
  $d$. So it remains to show that $d$ divides all the $r_j$.  By the the Division Theorem \ref{thm:division-theorem} 
  there exist $a_j, b_j \in \Z$, $j=1,\ldots,k$ with $0\leq b_j < d$ such that 
  $r_j = a_j\cdot d + b_j$. Assume that $b_i >0$. Then 
  \[
    b_i =r_i - a_i \cdot d = (1 -a_ix_i)r_i - \sum_{j\neq i} (a_i x_j) r_j ,
  \]
  where the $x_j \in \Z$ are chosen so that $d =x_1 r_1 + \ldots + x_kr_k$. But this means that $b_i \in L$ 
  which contradicts the minimality of $d$. Hence $b_j =0$ for $j=1,\ldots,k$, and $d$  divides all $r_j$.
\end{proof}

\begin{example}
  If $p$ is a prime number and $r$ an integer, then $p$ is a divisor of $r$ if and only if 
  $\gcd (p,r) = p$. Otherwise $\gcd (p,r) =1 $.  
  The greatest common divisor of $0$ and an integer $r$ is $|r|$, the greatest common divisor 
  of $1$ and an integer $r$ is $1$. 
\end{example}

\para With the proof of Proposition \ref{thm:existence-uniqueness-greatest-common-divisor} we have also shown the 
      following result. 

\begin{lemma}[B{\'e}zout's Lemma]\label{thm:bezouts-lemma}
  A natural number  is the greatest common divisor of integers $r_1,\ldots ,r_k$ if and only if it is the smallest 
  positive natural number $d$ for which there exist $x_1,\ldots ,x_k \in \Z$ such that
  \[
    d = x_1 r_1 + \ldots + x_kr_k \ .
  \]
\end{lemma}

\begin{lemma}[Euclid's Lemma]
\label{thm:euclids-lemma}
  If a prime number $p$ divides the product $rs$ of two integers $r$ and $s$, 
  then $p$  divides at least one of the integers $r$ and $s$.
\end{lemma}

\begin{proof}
  If $p$ divides $r$ we are done. So assume that $p$ does not divide $r$. Then $\gcd (p,r)=1$, hence by Bezout's Lemma 
  \ref{thm:bezouts-lemma} there exist $x,y \in \Z$ such that $xp + yr=1$. Multiplication by $s$ gives
  $s = xs p +  yrs$. The right side is divisible by $p$, hence  $p$ divides $s$. 
\end{proof}