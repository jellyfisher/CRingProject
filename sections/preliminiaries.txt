Authors {#section-authors .unnumbered}
=======

The following people have contributed to this work, in alphabetical
order:\

<span style="font-variant:small-caps;"></span>

Copyright {#section-copyright .unnumbered}
=========

Copyright 2011 – 2018 Akhil Mathew & Markus J. Pflaum. The copyright of
each chapter or section lies with its author(s); see the and pages for
authorships.

Permission is granted to copy, distribute and/or modify all parts of
this document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, no Front-Cover Texts, and no
Back-Cover Texts.

A copy of the GNU Free Documentation License, Version 1.3 license is
included in the section entitled .

Attribution {#section-attribution .unnumbered}
===========

The following table lists the authors and contributors for each chapter
or section of this monograph. If no source is mentioned, the
corresponding author(s) submitted their work to the CRing Project. All
contributions have been released under the GNU Free Documentation
License, Version 1.3, see .

[|l|X|X|X|]{} & & &\
Chapter/Section & Author(s) & Contributor(s) & Source\
& & &\
Category Theory& A. Mathew & &\
Number Systems & M.J. Pflaum & &\
General Topology & F. Latrémolière & M.J. Pflaum & ,\
