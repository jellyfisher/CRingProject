% Copyright 2016 Akhil Mathew, licensed under GNU FDL v1.3 
% main author: 
%   Akhil Mathew 
% contributions by: 
%   Markus J. Pflaum 
%
\section{(Co)Chain complexes and their (co) homology}
\sectionmark{Complexes and their (co)homology}

\subsec{Chain complexes}
The chain complex is the most fundamental construction in
homological algebra.

\begin{definition}\label{def:chain-complex} 
Let $R$ be a ring. A \emph{chain complex} (\emph{over} $R$) is a family
of (left) $R$-modules $ (C_k )_{k \in \Z}$ together with so-called 
\emph{boundary operators} $\partial_k:C_k\rightarrow C_{k-1}$, $k\in \Z$, such that
$\partial_{k-1}\partial_k=0$ for all $k\in \Z$. The boundary map $\partial$ is also
called the \emph{differential}. Often, notation is abused and the indices for
the boundary map are dropped. A chain complex is often simply denoted 
by $(C_\bullet,\partial)$ or even only by $C_\bullet$.

One calls a chain complex $C_\bullet$ \emph{bounded below} (respectively 
\emph{bounded above}) if there exists an $ n \in \Z$ such that 
$C_k = 0$ for all $k\leq n$ (respectively $C_k = 0$ for all $k\geq n$). 
If one has $C_k = 0$ for all $k < 0 $ (respectively $C_k = 0$ for all $k > 0 $),
the chain complex $C_\bullet$ is called \emph{positive} (respectively \emph{negative}). 
A chain complex $C_\bullet$ is called \emph{bounded} if it is both bounded below and 
bounded above. 
\end{definition}

\begin{example} 
Any family of $R$-modules $( C_k )_{k \in \Z}$ with the boundary operators
identically zero forms a chain complex. 

We will see plenty of more examples in due time.
\end{example}

\begin{proposition}
  If $(C_\bullet,\partial)$ is a chain complex, then 
  $\Img \partial_{k+1} \subset \Ker \partial_k$ for each $k\in \Z$.
\end{proposition}
\begin{proof}
   The claim is an immediate consequence of the relation  $\partial_k \partial_{k+1} =0$.
\end{proof}
%
The observation from the proposition leads us to the following definition.

\begin{definition} 
Let $(C_\bullet,\partial)$ be a chain complex. For each $k\in \Z$ one calls the module $C_k$ 
the module of $k$-\emph{chains}. The submodule of $k$-\emph{cycles} $Z_k\subset C_k$ is
the kernel $\Ker(\partial_k)$. The submodule of $k$-\emph{boundaries}
$B_k\subset C_k$ is the image $\Img(\partial_{k+1})$. The $k$-th \emph{homology group} of the complex 
$(C_\bullet,\partial)$ is now defined as the $R$-module $H_k(C_\bullet) := H_k(C_\bullet, \partial) := Z_k/B_k$.
The family $H_\bullet (C_\bullet) = \big( H_k(C_\bullet) \big)_{k\in \Z}$ is usually referred to as 
the \emph{homology} of $(C_\bullet,\partial)$.

A chain complex  $(C_\bullet,\partial)$ for which $Z_k = B_k$ or equivalently $H_k(C_\bullet)=0$ for every $k\in \Z$ is called 
\emph{exact}.
\end{definition}

\begin{remark} 
  In general, a chain complex need not be exact, and this failure of exactness is measured by
  its homology.
%\begin{environmentlist}
%\item
%   Homology is sometimes said to be the space of ``cycles mod boundaries''.    
%\end{environmentlist}
\end{remark}

\begin{examples} 
\begin{environmentlist}
\item
 In a chain complex $(C_\bullet, \partial)$ where all the boundary maps are
 trivial, i.e.~where $\partial =0$, one has $H_k(C_\bullet )=C_k$ for all $k\in \Z$. 
\item
 The homology $H_\bullet(C_\bullet )$ of a chain complex $C_\bullet$ can and will be understood 
 as a chain complex again with boundary maps being trivial. This interpretation 
 will be very useful when studying formality in rational or real homotopy theory,
 see \ref{chap:rational-homotopy-theory}.
\end{environmentlist}
\end{examples}

We have defined chain complexes now, but we have no notion of a morphism
between chain complexes yet. We do this next; it turns out that chain complexes 
form a category when morphisms are appropriately defined.

\begin{definition} \label{def:chain-maps}
 A \emph{morphism of chain complexes} (\emph{over the ring $R$}) 
 from $(C_\bullet,\partial)$ to $(D_\bullet,\delta)$
 or a \emph{chain map} is a family of $R$-module maps 
 $f_k: C_k\rightarrow D_k$, $k\in \Z$, such that 
 $f_{k-1} \partial_k = \delta_k f_k$ for all $k\in \Z$. In other words 
 this means that the diagram
 \[
 \begin{tikzcd}
   \arrow[r,dashed] & C_{k+1} \arrow[d,"f_{k+1}"] \arrow[r,"\partial_{k+1}"] &  C_k   \arrow[d,"f_k"] \arrow[r,"\partial_k"] &  
   C_{k-1} \arrow[d,"f_{k-1}"] \arrow[r,dashed] & \phantom{C}\\ 
   \arrow[r,dashed] & D_{k+1} \arrow[r,"\delta_{k+1}",swap] & D_k \arrow[r,"\delta_k",swap] & D_{k-1} \arrow[r,dashed] & \phantom{C}
  \end{tikzcd}
  \]
  commutes. We will  denote such a morphism of chain complexes by
  $f :(C_\bullet,\partial) \to (D_\bullet,\delta)$.
\end{definition}

\begin{remark}
  To further simplify notation, often all differentials regardless
  of what chain complex they are part of are denoted $\partial$,
  thus the commutativity relation on chain maps is simply
  $f\partial=\partial f$ with indices and distinction between the
  boundary operators dropped.
  Sometimes, though, when a distinction is really necessary, one
  writes $\partial^C$ or $\partial^D$ to denote the boundery map of
  $C_\bullet$ respectively $D_\bullet$. We will make sure in this book that 
  the context or the notation will always make clear what is meant. 
\end{remark}


\begin{propanddef}\label{thm:chain-complexes-category}
  Chain complexes over a ring $R$ together with their chain maps as morphisms become a 
  category which we denote by $\category{Ch}_\bullet (R\text{-}\category{Mod})$ or just  
  $\category{Ch}_\bullet$ when the ground ring $R$ is clear. The chain 
  complexes bounded below (respectively bounded above, bounded, positive, or negative)
  form a full subcategory of $\category{Ch}_\bullet (R\text{-}\category{Mod})$.  
  The resulting subcategories are denoted by 
  $\category{Ch}^+_\bullet (R\text{-}\category{Mod})$, $\category{Ch}^-_\bullet (R\text{-}\category{Mod})$,
  $\category{Ch}^\textup{b}_\bullet (R\text{-}\category{Mod})$, 
  $\category{Ch}^{\geq 0}_\bullet (R\text{-}\category{Mod})$,
  and $\category{Ch}^{\leq 0}_\bullet (R\text{-}\category{Mod})$, respectively.    
\end{propanddef}

\begin{proof}
  If $(C_\bullet,\partial)$ is a chain complex, then the family of identity maps $\id_{C_k}:C_k \to C_k$ 
  is clearly a chain map which we denote by $\id_{C_\bullet}$.
  If $f: (C_\bullet,\partial) \to  (D_\bullet,\delta)$ and  $g: (D_\bullet,\delta) \to  (E_\bullet,\varrho)$
  are chain maps, then $g \circ f : (C_\bullet,\partial) \to (E_\bullet,\varrho)$
  with components $(g \circ f)_k := g_k \circ f_k :C_k \to E_k$ is a chain map as well,
  since for all $k \in \Z$
  \[
    (g \circ f)_{k-1} \partial_k = g_{k-1} \circ f_{k-1} \circ \partial_k =  g_{k-1} \circ \delta_k \circ f_k = 
    \varrho_k \circ g_k \circ f_k =  \varrho_k ( g \circ f )_k \ .
  \] 
  Hence the chain complexes over the ring $R$ together with the chain maps form a category indeed.
  The rest of the claim is obvious.  
\end{proof}

\begin{proposition} 
A chain map $f : C_\bullet \to D_\bullet$ between chain complexes over a 
ring $R$ induces for each $k\in \Z$ a map in homology 
$H_k(f) : H_k(C_\bullet) \to H_k(D_\bullet)$. 
More precisely, each $H_k$ is a functor from chain complexes to $R$-modules, 
and homology becomes  a covariant functor from the category of chain complexes 
to the category of chain complexes with zero differential.  
\end{proposition}

\begin{proof}
Let $f :C_\bullet \rightarrow D_\bullet$ be a chain map. Let $\partial$ and
$\delta$ be the differentials for $C_\bullet$ and $D_\bullet$
respectively. Then we have a commutative diagram:

\[
\begin{tikzcd}
\arrow[r,dashed] & C_{k+1} \arrow[d,"f_{k+1}"] \arrow[r,"\partial_{k+1}"] &  C_k   \arrow[d,"f_k"] \arrow[r,"\partial_k"] &  
C_{k-1} \arrow[d,"f_{k-1}"] \arrow[r,dashed] & \phantom{C \ .}\\ 
\arrow[r,dashed] & D_{k+1} \arrow[r,"\delta_{k+1}",swap] & D_k \arrow[r,"\delta_k",swap] & D_{k-1} \arrow[r,dashed] & \phantom{C} \ .
\end{tikzcd}
\]

Now, in order to check that the chain map $f$ induces a map 
$H_k(f)$ on homology, we need to check that 
$f ( \Img (\partial))\subset \Img(\delta)$ and 
$f(\Ker(\partial))\subset \Ker(\delta)$. We first 
check the condition on images: we want 
to look at $f_k(\Img (\partial_{k+1}))$. By commutativity of $f$
and the boundary maps, this is equal to
$\delta_{k+1}(\Img (f_{k+1})$. Hence we have
$f_k(\Img (\partial_{k+1}))\subset \Img (\delta_{k+1})$. For the
condition on kernels, let $c\in \Ker(\partial_k)$. Then by
commutativity, $\delta_k(f_k(c))=f_{k-1}\partial_k(c)=0$.
Thus we have that $f$ induces for each $k \in \Z$ an $R$-module map
$H_k(f) : H_k(C_\bullet)\rightarrow H_k(D_\bullet)$. Hence it induces a
morphism on homology as a chain complex with zero differential. 
\end{proof}



\subsec{Long exact sequences}
{\bfseries add: OMG! We have all this and not the most basic theorem of them all.}

\begin{definition} If $M$ is a complex then for any integer $k$, we define a new complex $M[k]$ by shifting indices, i.e. $(M[k])^i:=M^{i+k}$.\end{definition}

\begin{definition} If $f:M\rightarrow N$ is a map of complexes, we define a complex $\mathrm{Cone}(f):=\{N^i\oplus M^{i+1}\}$ with differential
$$d(n^i,m^{i+1}):= (d_N^i(n_i)+(-1)^i\cdot f(m^{i+1}, d_M^{i+1}(m^{i+1}))$$
\end{definition}

Remark:  This is a special case of the total complex construction to be seen later.

\begin{proposition} A map $f:M\rightarrow N$ is a quasi-isomorphism if and only if $\mathrm{Cone}(f)$ is acyclic.\end{proposition}

\begin{proposition}  Note that by definition we have a short exact sequence of complexes
$$0\rightarrow N\rightarrow \mathrm{Cone}(f)\rightarrow M[1]\rightarrow 0$$
so by Proposition 2.1, we have a long exact sequence
$$\dots \rightarrow H^{i-1}(\mathrm{Cone}(f))\rightarrow H^{i}(M)\rightarrow H^{i}(N)\rightarrow H^{i}(\mathrm{Cone}(f))\rightarrow\dots$$
so by exactness, we see that $H^i(M)\simeq H^i(N)$ if and only if $H^{i-1}(\mathrm{Cone}(f))=0$ and $H^i(\mathrm{Cone}(f))=0$.  Since this is the case for all $i$, the claim follows. $\blacksquare$
\end{proposition}



\subsec{Cochain complexes}
Cochain complexes are much like chain complexes except the arrows point in the
opposite direction. Like before, $R$ denotes a fixed ring. 

\begin{definition}\label{def:cochain-complex}
A \emph{cochain complex} is a sequence of $R$-modules $(C^k)_{k \in \Z}$ with \emph{coboundary operators}, also
called \emph{differentials}, $d^k:C^k\rightarrow C^{k+1}$, $k\in \Z$, such that $d^{k+1}d^k=0$. 
A cochain copmplex is usually denoted by $(C^\bullet,d)$ or shortly by $C^\bullet$.

One calls a cochain complex $C^\bullet$ \emph{bounded below} (respectively 
\emph{bounded above}) if there exists an $ n \in \Z$ such that 
$C^k = 0$ for all $k\leq n$ (respectively $C^k = 0$ for all $k\geq n$). 
If one has $C^k = 0$ for all $k < 0 $ (respectively $C^k = 0$ for all $k > 0 $),
the cochain complex $C^\bullet$ is called \emph{positive} (respectively \emph{negative}). 
A cochain complex $C^\bullet$ which is both bounded below and bounded above is said to be \emph{bounded}. 

Let $(C^\bullet,d)$ and $(D^\bullet,\delta)$ denote cochain complexes. By a \emph{morphism of cochain complexes} 
or  a \emph{cochain map} from $(C^\bullet,d)$ to $(D^\bullet,\delta)$ we understand a family of
$R$-module maps $g^k : C^k \to D^k$, $k\in \Z$, such that $g^{k+1} d^k = \delta^k g^k$ for all  $k\in \Z$.
In other words this means we have a commutative diagram: 

\[
\begin{tikzcd}
\arrow[r,dashed] & C^{k-1} \arrow[d,"g^{k-1}"] \arrow[r,"d^{k-1}"] &  C^k   \arrow[d,"g^k"] \arrow[r,"d^k"] &  
C^{k+1} \arrow[d,"g^{k+1}"] \arrow[r,dashed] &\phantom{C \ .}\\ 
\arrow[r,dashed] & D^{k-1} \arrow[r,"\delta^{k-1}",swap] & D^k \arrow[r,"\delta^k",swap] & D^{k-1} \arrow[r,dashed]&\phantom{D} \ .
\end{tikzcd} 
\]
 We will  denote  such a morphism of cochain complexes usually by  $g :(C^\bullet,d) \to (D^\bullet, \delta)$.
\end{definition}

\begin{propanddef}\label{thm:chain-complexes-category}
  Cochain complexes over a ring $R$ together with their cochain maps as morphisms become a 
  category which we denote by $\category{Ch}^\bullet (R\text{-}\category{Mod})$ or just  
  $\category{Ch}^\bullet$ when the ground ring $R$ is clear. The cochain 
  complexes bounded below (respectively bounded above, bounded, positive, or negative)
  form a full subcategory of $\category{Ch}^\bullet (R\text{-}\category{Mod})$.  
  The corresponding subcategories are denoted by 
  $\category{Ch}_+^\bullet (R\text{-}\category{Mod})$, $\category{Ch}_-^\bullet (R\text{-}\category{Mod})$,
  $\category{Ch}_\textup{b}^\bullet (R\text{-}\category{Mod})$, 
  $\category{Ch}_{\geq 0}^\bullet (R\text{-}\category{Mod})$,
  and $\category{Ch}_{\leq 0}^\bullet (R\text{-}\category{Mod})$, respectively.    
\end{propanddef}

\begin{proof}
  The proof is completely dual to the proof of Proposition \ref{thm:chain-complexes-category}. 
\end{proof}


The theory of cochain complexes is entirely dual to that of chain complexes,
and we often shall not spell it out in detail.


For instance, we can form a category of cochain complexes and
\textbf{chain maps} (families of morphisms commuting with the
differential). Moreover, given a cochain complex $C^\bullet$, we
define the \textbf{cohomology objects} to be
$h^i(C^*)=\ker(\partial^i)/Im(\partial^{i-1})$; one obtains cohomology
functors.

It should be noted that the long exact sequence in cohomology runs in the
opposite direction. 
If $0 \to C_*' \to C_* \to C_*'' \to 0$ is a short exact sequence of cochain
complexes, we get a long exact sequence
\[ \dots \to H^i(C' ) \to H^i(C) \to H^{i}(C'') \to H^{i+1}(C' ) \to H^{i+1}(C) \to
\dots.  \]

Similarly, we can also turn cochain complexes and cohomology
modules into a
graded module.

Let us now give a standard example of a cochain complex.

\begin{example}[The de Rham complex] Readers unfamiliar with differential
forms may omit this example. Let $M$ be a smooth manifold. For each $p$, let
$C^p(M)$ be the $\R$-vector space of smooth $p$-forms on $M$.
We can make the $\left\{C^p(M)\right\}$ into a complex by defining the maps
\[ C^p(M) \to C^{p+1}(M)  \]
via $\omega \to d \omega$, for $d$ the exterior derivative. 
(Note that $d^2 = 0$.)  This complex is called the \textbf{de Rham complex} of
$M$, and its cohomology is called the \textbf{de Rham cohomology.} It is known
that the de Rham cohomology is isomorphic to singular cohomology with real
coefficients, cf.~\cite{War} and \cite{HatAT}.
\end{example} 
