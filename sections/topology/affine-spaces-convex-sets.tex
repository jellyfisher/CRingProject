% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum 
%
\section{Affine spaces and convex sets}

\begin{definition}
  By an \emph{affine space} over a field $\fldk$ one understands
  a set $\A$ together with a free and transitive action 
  \[
   \alpha : V \times \A \to \A, \quad (P,v) \mapsto v + P
  \]
  of a $\fldk$-vector space $V$ on $\A$. This means that $\alpha$ satisfies 
  the following relations:
  \begin{axiomlist}[Act]
  \item The zero element acts as identity that is $ 0 + P = P$ for all
        $P\in \A$.
  \item The map $\alpha$ is compatible with addition in $V$ which means that 
        $v + (w +P) = (v+w) + P$ for all $P\in \A$ and $v,w\in V$.   
  \item[{\sffamily (ActF)}\!]
        The action is free which means that for all $P\in \A$ and $v\in V$ the 
        relation $v + P = P$ entails $v=0$. 
  \item[{\sffamily (ActT)}\!] The action is transitive which means that for all
        elements $P,Q \in \A$ there exists an element $v \in V$ such that 
        $v + P = Q$. 
  \end{axiomlist}
  One calls the elements of $\A$ the \emph{points} of the affine space,
  and $V$ its \emph{vector space of translations}.
\end{definition}
 

\para 
  Since the action of $V$  on $\A$ is  free and transitive, one has a map
   \[
     - :\:  \A \times \A \to V , \quad (P,Q) \mapsto P - Q 
   \]
   which associates to each $(P,Q)\in  \A \times \A$ the unique vector 
   $v \in V$ such that $P = v + Q$. It is called \emph{subtraction map}.

\begin{proposition}
   The subtraction map of an affine space $(\A,V,\alpha)$ has the following 
   properties, called \emph{Weyl's axioms}:
   \begin{axiomlist}[Weyl]
   \item 
     For every $P\in \A$ the map $V \to \A$, $v \mapsto v + P$ is 
     a bijection.
   \item
     For all $P,Q,R\in \A$ one has
     \[
         (P-Q) + (Q-R) = P-R \ .
     \]  
   \end{axiomlist}
\end{proposition}

\begin{definition}
  If $(\A,V,\alpha)$ and $(\B,W,\beta)$ are two affine spaces, an
  \emph{affine map} or \emph{affine homomorphism} between them is a map
  $f: \A \to \B$  such that the function
  \[
    \A \times \A \to W, \quad (P,Q) \mapsto f(P) - f(Q) 
  \]
  factors through a linear map which means that there exists a linear map 
  $F : V \to W$ such that $F(P-Q)= f(P) - f(Q)$ for all $P,Q \in \A$.    
\end{definition}

\begin{remark}
  The linear map $F$ associated to an affine map $f$ is uniquely determined. 
  
\end{remark}

\begin{example}
  Let $V$ be a $\fldk$-vector space. Put $\A_V := V$ and let the action
  $\alpha : V \times \A_V \to \A_V$ coincide with addition. Then
  $(\A_V ,V,\alpha)$ is an affine space called the 
  the affine \emph{associated} to the vector space $V$.
\end{example}