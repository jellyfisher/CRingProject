% Copyright 2016 Markus J. Pflaum, licensed under GNU FDL v1.3
% main author: 
%   Markus J. Pflaum
%
\section{Separation properties}
\para 
The general definition of a topology allows for examples where elements of a 
topological space, seen as a set, can not be distinguished from each other 
by open sets (for instance if the topology is indiscrete). When points can be 
topologically  differentiated, a topology is in some sense separated. The 
standard separation axioms allow to subsume topological spaces with certain
separability properties in particular classes. One then studies the
properties of these clases, often with a view to particular applications, and 
attempts to create counter examples, meaning examples not satsifying the
corresponding separation axioms.
The most important separability property goes back to the founder of 
set-theoretic topology, Felix Hausdorff, who introduced it in 1914.
The first full presentation of the separation axioms as we know them today 
appeared in the classic book \emph{Topologie} by \cite{AleHopT}
under their German name \emph{Trennungsaxiome}. 

Let us note that the literature on separation axioms is not uniform
when it comes to the axioms \ref{axiom:t3} to \ref{axiom:t6} below,
so one needs to always check which convention an author follows. Here, we follow
the convention by \cite[Part I, Chap.~2]{SteSeeCT} which coincides with the one of 

\begin{definition}[The Separation Axioms]
  Recall that two subsets $A,B$ of a topological space $(X,\topology)$
  are called \emph{disjoint} if $A\cap B = \emptyset$. The two sets are called
  \emph{separated} if $ \closure{A} \cap B = A \cap \closure{B} = \emptyset$.
  The topological space $(X,\topology)$ now is said to be 
  \begin{axiomlistphantom}[T]{2.3mm}
  \setcounter{enumi}{-1}
  \item \label{axiom:t0}
        or \emph{Kolmogorov} 
        if for each pair   of distinct points $x,y \in X$ there is
        an open $U\subset X$ such that $x\in U$ and $y \notin U$ 
        holds true, or  $y\in U$ and $x \notin U$,
  \item \label{axiom:t1}
        or \emph{Fr\'echet} 
        if for each pair of distinct points $x,y \in X$ there is
        an open $U\subset X$ such that $x\in U$ and $y \notin U$,
  \item \label{axiom:t2}
        or \emph{Hausdorff} 
        if for each pair of distinct points $x,y \in X$ there exist
        disjoint open sets $U,V \subset X$ such that $x\in U$ and $y \in V$,
  \item[{\rmfamily (}{\sffamily T2$_{\mathsf{\frac 12}}${\rmfamily )}}] 
        or \emph{Uryson} or \emph{completely Hausdorff} 
        if for each pair of distinct points $x,y \in X$ there exist
        distinct closed neigborhoods $U$ of $x$ and $V$ of $y$,
  \item \label{axiom:t3}
        if for each point $x \in X$ and closed subset $A \subset X$
        with  $x \notin A$ there exist disjoint open sets $U,V \subset X$ 
        such that $x\in U$ and $A \subset V$,
  \item[{\rmfamily (}{\sffamily T3$_{\mathsf{\frac 12}}${\rmfamily )}}] \label{axiom:t3a} 
        if for each point $x \in X$ and closed subset $A \subset X$
        with  $x \notin A$ there exists a continuous function $f: X \to \R$ 
        such that $f(x)=0$ and $f (A) = \{ 1 \}$,
  \item \label{axiom:t4}
        if for each pair of closed disjoint subsets $A,B \subset X$
        there exist disjoint open sets $U,V \subset X$ such that 
        $A \subset U$ and $B \subset V$,
  \item \label{axiom:t5}
        if for each pair of separated subsets $A,B \subset X$
        there exist disjoint open sets $U,V \subset X$ such that 
        $A \subset U$ and $B \subset V$,
  \item \label{axiom:t6}
        if for each pair of disjoint closed subsets $A,B \subset X$
        there exists a continuous function  $f: X \to \R$ 
        such that $A = f^{-1} (0)$  and $B = f^{-1}(0)$.
  \end{axiomlistphantom}
  A Hausdorff  space will be called \emph{regular}  
  if it fulfills \ref{axiom:t3},  \emph{completely regular}, if it 
  satisfies \ref{axiom:t3a}, and \emph{normal} if \ref{axiom:t4} holds true.  
  Finally we call a Hausdorff space \emph{completely normal} if it is \ref{axiom:t5} 
  and \emph{perfectly normal} if it is \ref{axiom:t6}.     
\end{definition}